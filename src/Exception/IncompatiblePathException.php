<?php

/*
 * This file is part of the Raini Develop package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Dev\Exception;

use Raini\Core\File\PathInfo;

/**
 * Exception to be thrown when path targets for a command are incompatible.
 *
 * This occurs when a coding standard, code analysis or set of tests are
 * incompatible between all the paths. For instance a vendor package and an
 * Drupal module will specify different coding standards and cannot be run
 * in a single command invokation.
 */
class IncompatiblePathException extends \Exception
{

    /**
     * @param string          $name
     * @param string|PathInfo $path
     * @param int             $code
     * @param \Throwable|null $prev
     */
    public function __construct(string $name, string|PathInfo $path, int $code = 0, ?\Throwable $prev = null)
    {
        $message = sprintf('The path "%s" is incompatible with %s', $path, $name);

        parent::__construct($message, $code, $prev);
    }
}
