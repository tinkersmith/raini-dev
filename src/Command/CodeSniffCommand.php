<?php

/*
 * This file is part of the Raini Develop package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Dev\Command;

use Raini\Core\Command\Option\OptionFactoryInterface;
use Raini\Core\File\PathResolver;
use Raini\Core\Devel\CodeStandardManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Process\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Tinkersmith\Console\Exception\CliRuntimeException;

/**
 * Command to run PHP code sniffer on a PHP package or module.
 */
#[AsCommand(
    name: 'lint:php',
    description: 'Use PHP CodeSniffer to validate PHP, YAML, Javascript, and CSS files against a provided coding standard.',
    aliases: ['cs'],
)]
class CodeSniffCommand extends AbstractTenantPathCommand
{

    /**
     * @param string|string[]              $defaultPath
     * @param PathResolver                 $pathResolver
     * @param OptionFactoryInterface       $optionFactory
     * @param CodeStandardManagerInterface $codeStandards
     */
    public function __construct(protected string|array $defaultPath, protected PathResolver $pathResolver, protected OptionFactoryInterface $optionFactory, protected CodeStandardManagerInterface $codeStandards)
    {
        parent::__construct($defaultPath, $pathResolver, $optionFactory);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        parent::configure();

        $standards = $this->codeStandards->getAvailableStandards();
        $labels = implode('", "', array_column($standards, 'label'));
        $this->setHelp(<<<EOT
            Validate PHP, CSS and JavaScript against a coding standard with
            PHP Codesniffer. Raini extensions are able to add more standards and
            influence which standards are used based path information.

            Available standards:
              <info>"$labels"</info>

            The Raini version of the command has fewer options and
            does not support interactive mode, but is understands how to resolve
            Raini project paths.

            This command can automatically select the coding standard based on the
            path and path type provided. For instance "vendor:raini/core" will
            target the Raini package in the vendor directory with the "Symfony"
            standard.
            EOT);

        $this->addOption('standard', null, InputOption::VALUE_REQUIRED, 'The coding standard to validate against.');
        $this->addOption('generator', null, InputOption::VALUE_REQUIRED, 'Use either the "HTML", "Markdown" or "Text" generator (forces documentation generation instead of checking).');
        $this->addOption('ignore', null, InputOption::VALUE_REQUIRED, 'Comma separated list of patterns of files to ignore.');
        $this->addOption('fix', 'f', InputOption::VALUE_NONE, 'Attempt to autofix code formatting errors.');
        $this->addOption('explain', null, InputOption::VALUE_NONE, 'Explain the standard by showing the sniffs it includes.');
    }

    /**
     * {@inheritdoc}
     */
    protected function getPathDescription(): string
    {
        return 'The path to the code to run the code sniff on.';
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            @['tenant' => $tenant, 'environment' => $environment] = $this->options->getValues($input);

            if ($paths = $this->resolvePaths($input->getArgument('path'), $tenant)) {
                // Check and apply the coding standard requested or guess which one
                // to use from based on the target path.
                if ($standard = $input->getOption('standard')) {
                    $standard = strtolower($standard);
                    $handler = $this->codeStandards->getHandlerByStandard($standard);
                } elseif ($standardInfo = $this->codeStandards->getStandardByPath($paths, $tenant)) {
                    [$standard, $handler] = $standardInfo;
                } else {
                    exit(sprintf('Unable to determine a coding standard for the path: %s', (string) $paths[0]));
                }

                $standardDef = $handler->getInfo()[$standard];
                $output->writeln("Using standard: <info>{$standardDef['label']}</info> at <info>".implode(', ', $paths).'</info>');

                return $this->codeStandards->execute($paths, $tenant, $handler, $standard, $input->getOptions(), $environment);
            }
        } catch (RuntimeException|CliRuntimeException $e) {
            if ($output->isVerbose()) {
                $this->getApplication()->renderThrowable($e, $output);
            }

            return $e->getCode();
        }

        // The command failed.
        return 128;
    }
}
