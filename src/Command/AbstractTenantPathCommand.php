<?php

/*
 * This file is part of the Raini Develop package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Dev\Command;

use Raini\Core\Command\Option\EnvironmentTenantOption;
use Raini\Core\Command\Option\OptionCollection;
use Raini\Core\Command\Option\OptionFactoryInterface;
use Raini\Core\File\Exception\InvalidPathException;
use Raini\Core\File\PathInfo;
use Raini\Core\File\PathResolver;
use Raini\Core\Project\Tenant;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Base class for Development commands that utilize a target path to execute on.
 */
abstract class AbstractTenantPathCommand extends Command
{

    /**
     * A collection of command option providers.
     *
     * The option providers add the command line option information and
     * resolution into their respective instances. E.G. Rather than just getting
     * the string value for environment from the command input object, the
     * EnvironmentOption provider will validate (including throwing errors)
     * and provide the EnvironmentInterface object that matches the value.
     *
     * @var OptionCollection
     */
    protected OptionCollection $options;

    /**
     * @param string|string[]        $defaultPath
     * @param PathResolver           $pathResolver
     * @param OptionFactoryInterface $optionFactory
     */
    public function __construct(protected string|array $defaultPath, protected PathResolver $pathResolver, OptionFactoryInterface $optionFactory)
    {
        $this->options = $optionFactory->createCollection([
            'tenant' => EnvironmentTenantOption::class,
        ]);

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this->addArgument('path', InputArgument::OPTIONAL|InputArgument::IS_ARRAY, $this->getPathDescription());

        $this->options->apply($this);
    }

    /**
     * Get the description for the command path argument.
     *
     * @return string The description to use for the command path argument.
     */
    abstract protected function getPathDescription(): string;

    /**
     * Transform the path argument into any array of PathInfo objects.
     *
     * This method ensures that paths are resolved and translated into PathInfo
     * instances. This includes applying the default paths for the command if
     * the path argument is empty.
     *
     * @param string[]|string|null $paths  The path arguments from the command line.
     * @param Tenant               $tenant The tenant to use as reference for path resolution.
     *
     * @return PathInfo[] The command path argument transformed into an array of PathInfo objects.
     */
    protected function resolvePaths(array|string|null $paths, Tenant $tenant): array
    {
        if (!$paths) {
            $paths = $this->pathResolver->isBasePath() ? $this->defaultPath : '';
        }
        if (!is_array($paths)) {
            $paths = [$paths];
        }

        $pathInfo = [];
        foreach ($paths as $path) {
            if ($info = $this->pathResolver->resolve($path, $tenant)) {
                $pathInfo[] = $info;
            } else {
                throw new InvalidPathException($path);
            }
        }

        return $pathInfo;
    }
}
