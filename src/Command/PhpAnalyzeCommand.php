<?php

/*
 * This file is part of the Raini Develop package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Dev\Command;

use Raini\Core\Command\Option\OptionFactoryInterface;
use Raini\Core\Devel\AnalyzerManagerInterface;
use Raini\Core\File\Exception\InvalidCommandPathException;
use Raini\Core\File\PathResolver;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\RuntimeException;
use Tinkersmith\Console\Exception\CliRuntimeException;

/**
 * Command to run PHP static analysis (PHPStan) tools on files or folders.
 */
#[AsCommand(
    name: 'php:analyze',
    description: 'Runs PHP code analysis (PHPStan) on PHP resources, files or folders.',
    aliases: ['analyze', 'phpstan'],
)]
class PhpAnalyzeCommand extends AbstractTenantPathCommand
{

    /**
     * @param string|string[]          $defaultPath
     * @param PathResolver             $pathResolver
     * @param OptionFactoryInterface   $optionFactory
     * @param AnalyzerManagerInterface $analyzerManager
     */
    public function __construct(protected string|array $defaultPath, protected PathResolver $pathResolver, OptionFactoryInterface $optionFactory, protected AnalyzerManagerInterface $analyzerManager)
    {
        parent::__construct($defaultPath, $pathResolver, $optionFactory);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        parent::configure();
        $this->setHelp(<<<EOT
            Command runs static code analysis on a target path.
            EOT);

        $this
            ->addOption('format', 'f', InputOption::VALUE_OPTIONAL, 'Output format for errors (table [default], raw, json, junit, checkstyle, none).')
            ->addOption('no-progress', null, InputOption::VALUE_NONE, 'Do not show progress bar, only output results.')
            ->addOption('deprecations', 'd', InputOption::VALUE_NEGATABLE, 'Include/Exclude code depreciations in output.', true);
    }

    /**
     * {@inheritdoc}
     */
    protected function getPathDescription(): string
    {
        return 'The path to run the PHP static analysis command on.';
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            @['tenant' => $tenant, 'environment' => $environment] = $this->options->getValues($input);

            $paths = $this->resolvePaths($input->getArgument('path'), $tenant);
            if ($analyzer = $this->analyzerManager->getAnalyzerByPath($paths, $tenant)) {
                return $analyzer->execute($paths, $tenant, $environment, $input->getOptions());
            }

            throw new InvalidCommandPathException($this, reset($paths));
        } catch (RuntimeException|CliRuntimeException $e) {
            if ($output->isVerbose()) {
                $this->getApplication()->renderThrowable($e, $output);
            }

            return $e->getCode();
        }
    }
}
