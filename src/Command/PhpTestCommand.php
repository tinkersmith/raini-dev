<?php

/*
 * This file is part of the Raini Develop package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Dev\Command;

use Raini\Core\Command\Option\OptionFactoryInterface;
use Raini\Core\File\Exception\InvalidCommandPathException;
use Raini\Core\File\PathResolver;
use Raini\Core\Test\TestManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\RuntimeException;
use Tinkersmith\Console\Exception\CliRuntimeException;

/**
 * Command to run automated testing suites configured for the project.
 *
 * Available test suites are registered to the "@php_test.manager" service.
 */
#[AsCommand(
    ' php:test',
    'Runs PHPUnit tests on Raini extensions or run test suites provided by extensions.',
    ['phpunit', 'php:test']
)]
class PhpTestCommand extends AbstractTenantPathCommand
{

    /**
     * @param string|string[]        $defaultPath
     * @param PathResolver           $pathResolver
     * @param OptionFactoryInterface $optionFactory
     * @param TestManagerInterface   $testManager
     */
    public function __construct(protected string|array $defaultPath, protected PathResolver $pathResolver, OptionFactoryInterface $optionFactory, protected TestManagerInterface $testManager)
    {
        parent::__construct($defaultPath, $pathResolver, $optionFactory);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        parent::configure();
        $this->setHelp(<<<EOT
            Command runs PHP Unit test on a specified target.
            EOT);
    }

    /**
     * {@inheritdoc}
     */
    protected function getPathDescription(): string
    {
        return 'The path to run the PHP Unit tests on.';
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            @['tenant' => $tenant, 'environment' => $environment] = $this->options->getValues($input);

            $paths = $this->resolvePaths($input->getArgument('path'), $tenant);
            if ($tester = $this->testManager->getTesterByPath($paths, $tenant)) {
                return $tester->execute($paths, $tenant, $environment);
            }

            throw new InvalidCommandPathException($this, reset($paths));
        } catch (RuntimeException|CliRuntimeException $e) {
            if ($output->isVerbose()) {
                $this->getApplication()->renderThrowable($e, $output);
            }

            return $e->getCode();
        }
    }
}
