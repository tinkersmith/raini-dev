<?php

/*
 * This file is part of the Raini Develop package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Dev\Project\Generator;

use Raini\Core\Project\GenerateOptions;
use Raini\Core\Environment;
use Raini\Core\Project\Tenant;
use Raini\Core\Project\TenantGeneratorInterface;
use Raini\Dev\Event\AnalyzeSettingsAlterEvent;
use Raini\Dev\Event\DevGeneratorEvents;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Generator method to create the development settings files.
 */
class DevConfigGenerator implements TenantGeneratorInterface
{

    /**
     * @param Environment              $env
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(protected Environment $env, protected EventDispatcherInterface $eventDispatcher)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function isApplicable(Tenant $tenant, GenerateOptions $options): bool
    {
        // Always create a PHPStan file for the PHP project.
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function runForTenant(Tenant $tenant, GenerateOptions $options, ?OutputInterface $output = null): void
    {
        // Generate the phpstan.neon settings for the project.
        $filename = $tenant->getBasePath().'./phpstan.neon';
        $settings = file_exists($filename) ? Yaml::parseFile($filename) : $this->getDefaultPhpStanSettings();
        $vendorDir = $tenant->getVendorDir();

        $files = [];

        if (file_exists("{$vendorDir}/itamair/geophp/geoPHP.inc")) {
            $files[] = "{$vendorDir}/itamair/geophp/geoPHP.inc";
        }

        foreach ($files as $file) {
            // Add the bootstrap file if it isn't already included.
            if (file_exists($file) && false === array_search($file, $settings['parameters']['bootstrapFiles'] ?? [])) {
                $settings['parameters']['bootstrapFiles'][] = $file;
            }
        }

        // Allow other extensions to alter the PHPStan command settings file.
        $event = new AnalyzeSettingsAlterEvent($tenant, $settings);
        $this->eventDispatcher->dispatch($event, DevGeneratorEvents::PHP_ANALYZE_SETTINGS_ALTER);

        file_put_contents('./phpstan.neon', Yaml::dump($settings, 4));
    }

    /**
     * Get the default PHP static analysis (PHPStan) settings.
     *
     * @return mixed[] Default PHPStan settings for the "phpstan.neon" file.
     */
    public function getDefaultPhpStanSettings(): array
    {
        return [
            'includes' => [
                'phar://phpstan.phar/conf/bleedingEdge.neon',
            ],
            'parameters' => [
                'level' => 5,
                'paths' => ['src'],
                'excludePaths' => [
                    '*/bower_components/*',
                    '*/node_modules/*',
                ],
                'bootstrapFiles' => [],
                'earlyTerminatingFunctionCalls' => ['throwException'],
            ],
        ];
    }
}
