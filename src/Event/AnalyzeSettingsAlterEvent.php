<?php

/*
 * This file is part of the Raini Develop package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Dev\Event;

use Raini\Core\Event\RainiEvent;
use Raini\Core\Project\Tenant;

/**
 * Event for altering the PHP analysis project settings.
 *
 * Alters the "phpstan.neon" file values that get written to the project
 * directory during a "project:generate" command.
 *
 * @see \RainiDev\Event\DevGeneratorEvents::PHP_ANALYZE_SETTINGS_ALTER
 */
class AnalyzeSettingsAlterEvent extends RainiEvent
{

    /**
     * @param Tenant  $tenant          The tenant that the PHPStan settings are built for.
     * @param mixed[] $analyzeSettings Reference to the "phpstan.neon" definition to be altered.
     */
    public function __construct(protected Tenant $tenant, protected array &$analyzeSettings)
    {
    }

    /**
     * @return Tenant The tenant that the PHPStan configurations are being built for.
     */
    public function getTenant(): Tenant
    {
        return $this->tenant;
    }

    /**
     * @return mixed[] Get a reference to the current full "phpstan.neon" definitions.
     */
    public function &getAnalyzeSettings(): array
    {
        return $this->analyzeSettings;
    }

    /**
     * @return mixed[] Get a reference to the current "phpstan.neon" parameters.
     */
    public function &getAnalyzeParameters(): array
    {
        return $this->analyzeSettings['parameters'];
    }
}
