<?php

/*
 * This file is part of the Raini Develop package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Dev\Event;

/**
 * Raini Develop package generator events.
 */
final class DevGeneratorEvents
{

    /**
     * Event to add and/or alter the project PHPStan settings.
     *
     * The settings are written to a "phpstan.neon" file in the project
     * directory and is the default configurations for the "php:analyze"
     * command.
     *
     * The analyze settings alter event uses the event class:
     *   \RainiDev\Event\AnalyzeSettingsAlterEvent
     *
     * @see \RainiDev\Event\AnalyzeSettingsAlterEvent
     */
    public const PHP_ANALYZE_SETTINGS_ALTER = 'raini_dev:analyze_settings_alter';
}
