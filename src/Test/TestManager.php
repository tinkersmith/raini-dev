<?php

/*
 * This file is part of the Raini Develop package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Dev\Test;

use Raini\Core\DependencyInjection\PrioritizedCollectorTrait;
use Raini\Core\Test\TesterInterface;
use Raini\Core\Test\TestManagerInterface;
use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;
use Raini\Dev\Exception\IncompatiblePathException;

/**
 * Service collector to manage Tester implementations (to run tests).
 */
class TestManager implements TestManagerInterface
{

    use PrioritizedCollectorTrait;

    protected string $interface = TesterInterface::class;

    /**
     * {@inheritdoc}
     */
    public function getTesterByPath(PathInfo|array $paths, Tenant $tenant): ?TesterInterface
    {
        if (!is_array($paths)) {
            $paths = [$paths];
        }

        $path = reset($paths);
        foreach ($this->handlers as $def) {
            if ($def['handler']->isPathApplicable($path, $tenant)) {
                // Ensure all the paths can be handled by the same handler,
                // otherwise there might be a mismatch with test framework.
                while (false !== ($path = next($paths))) {
                    if (!$def['handler']->isPathApplicable($path, $tenant)) {
                        throw new IncompatiblePathException($path, $def['handler']->getName());
                    }
                }

                return $def['handler'];
            }
        }

        return null;
    }
}
