<?php

/*
 * This file is part of the Raini Develop package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Dev\Test;

use Tinkersmith\Console\ExecutionContextInterface;
use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;
use Raini\Core\Test\TesterInterface;

/**
 * Tester implementation to run on Raini core, droplets, and extensions.
 */
class RainiTester implements TesterInterface
{

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'Raini tester';
    }

    /**
     * {@inheritdoc}
     */
    public function getServiceId(): string
    {
        return 'raini';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(): string
    {
        return 'Run tests on Raini extensions';
    }

    /**
     * {@inheritdoc}
     */
    public function isPathApplicable(PathInfo $path, Tenant $tenant): bool
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(PathInfo|array $path, Tenant $tenant, EnvironmentInterface|ExecutionContextInterface $context): int
    {
        return 0;
    }
}
