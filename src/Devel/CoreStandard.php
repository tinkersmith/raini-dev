<?php

/*
 * This file is part of the Raini Develop package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Dev\Devel;

use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\Environment;
use Raini\Core\Devel\CodeStandardInterface;
use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;
use Tinkersmith\Console\Output\BufferedOutput;

/**
 * The core coding standard implementation.
 *
 * Provides information about coding standards that come with CodeSniffer and
 * adds the Symfony validations rules as well. The "Symfony" standard is used
 * for Composer packages in the "vendor" folder by default.
 */
class CoreStandard implements CodeStandardInterface
{

    /**
     * List of codign standard data listed from CodeSniffer.
     *
     * @var string[]|null
     */
    protected ?array $standards;

    /**
     * @param Environment         $env
     * @param CliFactoryInterface $cliFactory
     */
    public function __construct(protected Environment $env, protected CliFactoryInterface $cliFactory)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getInfo(): array
    {
        if (!isset($this->standards)) {
            $buffer = new BufferedOutput();
            $this->cliFactory
                ->create($this->env->getBinPath().'/phpcs')
                ->execute(['-i'], $buffer);

            // Break up the string, and filter out the coding standards.
            [$intro, $tmp] = preg_split('/,\s*/', $buffer->fetch(), 2);
            $words = explode(' ', $intro);
            $info = preg_split('/(,\s*|\s+and\s+)/i', $tmp);
            array_unshift($info, end($words));

            foreach ($info as $name) {
                $this->standards[strtolower($name)] = [
                    'label' => $name,
                    'value' => $name,
                ];
            }

            // Include the Symfony standard if it has been installed. It is a
            // requirement of Raini Dev so should be available.
            $symfonyPath = $this->env->getVendorDir().'/escapestudios/symfony2-coding-standard/Symfony';
            if (!isset($info['symfony']) && is_readable($symfonyPath)) {
                $this->standards['symfony'] = [
                    'label' => 'Symfony',
                    'value' => $symfonyPath,
                ];
            }

            if (!empty($this->standards['symfony'])) {
                $this->standards['symfony']['defaultPath'] = 'vendor:tinkersmith';
            }
        }

        return $this->standards;
    }

    /**
     * {@inheritdoc}
     */
    public function getStandardByPath(PathInfo $info, Tenant $tenant): ?string
    {
        if ('vendor' === $info->getType() || (PathInfo::PROJECT_PATH === $info->getType() && str_starts_with($this->env->getVendorDir(), $info->path))) {
            return 'symfony';
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getCommand(Tenant $tenant, array $options = []): string|array
    {
        $cmd = empty($options['fix']) ? 'phpcs' : 'phpcbf';

        return $this->env->getBinPath()."/$cmd";
    }

    /**
     * {@inheritdoc}
     */
    public function getStandardValue(string $standard, Tenant $tenant): string
    {
        $standards = $this->getInfo();

        return ($standards[$standard] ?? $standards['symfony'])['value'];
    }
}
