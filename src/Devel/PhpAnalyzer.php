<?php

/**
 * This file is part of the Raini Develop package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Dev\Devel;

use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\Devel\AnalyzerInterface;
use Raini\Core\Environment;
use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\File\PathHelper;
use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;
use Tinkersmith\Console\ExecutionContextInterface;

/**
 * Should be placed last analyzer to run and used as the fallback.
 *
 * The fallback, should be the last analyzer and will run standard 'phpstan'
 * with project defaults.
 */
class PhpAnalyzer implements AnalyzerInterface
{

    /**
     * @param Environment         $env
     * @param CliFactoryInterface $cliFactory
     */
    public function __construct(protected Environment $env, protected CliFactoryInterface $cliFactory)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'Default Analyzer';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(): string
    {
        return 'Runs PHP static code analyzer with project defaults.';
    }

    /**
     * {@inheritdoc}
     */
    public function isPathApplicable(PathInfo $path, Tenant $tenant): bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(array|PathInfo $paths, Tenant $tenant, EnvironmentInterface|ExecutionContextInterface $context, array $options): int
    {
        // Run PHPStan using the Symfony standard at the base project.
        $cli = $this->cliFactory->create($this->env->getBinPath().'/phpstan', $context);
        $args = ['analyze'];

        // Apply command options for the PHPStan execution.
        if (!empty($options['format'])) {
            $args[] = '--error-format='.$options['format'];
        }
        if (!empty($options['no-progress'])) {
            $args[] = '--no-progress';
        }

        // Add the path to execute the code analysis on.
        $args[] = '--';

        if (!is_array($paths)) {
            $paths = [$paths];
        }

        foreach ($paths as $path) {
            // PHPStan has a bug when the target path is symlink.
            // Resolve the symlink and pass this as the PHPStan path argument.
            // @see https://github.com/phpstan/phpstan/issues/6585
            if (is_link($path)) {
                $path = dirname($path).'/'.readlink($path);
                $path = PathHelper::reducePath($path);
            }
            $args[] = $path;
        }

        return $cli
            ->setTty(true)
            ->execute($args);
    }
}
