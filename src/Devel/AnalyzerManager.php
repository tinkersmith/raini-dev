<?php

/**
 * This file is part of the Raini Develop package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Dev\Devel;

use Raini\Core\DependencyInjection\PrioritizedCollectorTrait;
use Raini\Core\Devel\AnalyzerInterface;
use Raini\Core\Devel\AnalyzerManagerInterface;
use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;
use Raini\Dev\Exception\IncompatiblePathException;

/**
 * The default code analyzer manager.
 *
 * The manager is a responsible for collecting, selecting and initialized the
 * analyzer to use based on the context.
 */
class AnalyzerManager implements AnalyzerManagerInterface
{

    use PrioritizedCollectorTrait;

    /**
     * {@inheritdoc}
     */
    public function getAnalyzerByPath(PathInfo|array $paths, Tenant $tenant): ?AnalyzerInterface
    {
        if (!is_array($paths)) {
            $paths = [$paths];
        }

        $path = reset($paths);
        /** @var AnalyzerInterface $analyzer */
        foreach ($this->getSortedHandlers() as $analyzer) {
            if ($analyzer->isPathApplicable($path, $tenant)) {
                // Ensure all the paths can be handled by the same handler,
                // otherwise there might be a mismatch with test framework.
                while (false !== ($path = next($paths))) {
                    if (!$analyzer->isPathApplicable($path, $tenant)) {
                        throw new IncompatiblePathException($path, $analyzer->getName());
                    }
                }

                return $analyzer;
            }
        }

        return null;
    }
}
