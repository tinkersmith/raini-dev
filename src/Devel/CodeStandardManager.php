<?php

/*
 * This file is part of the Raini Develop package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Dev\Devel;

use Raini\Core\Console\CliFactoryInterface;
use Raini\Core\DependencyInjection\PrioritizedCollectorTrait;
use Raini\Core\Devel\CodeStandardInterface;
use Raini\Core\Devel\CodeStandardManagerInterface;
use Raini\Core\Devel\ExecutableStandardInterface;
use Raini\Core\Environment\EnvironmentInterface;
use Raini\Core\File\PathInfo;
use Raini\Core\Project\Tenant;
use Raini\Dev\Exception\IncompatiblePathException;
use Tinkersmith\Console\ExecutionContextInterface;

/**
 * The code standard manager.
 *
 * A service collector for services tagged as a "code_standard". The code
 * standard manager is able to report about what coding standards are available
 * and guess a standard to use based on the target scanning path.
 */
class CodeStandardManager implements CodeStandardManagerInterface
{

    use PrioritizedCollectorTrait {
        addHandler as protected traitAddHandler;
    }

    /**
     * All collected services should implement the CodeStandardInterface.
     *
     * @var string|null
     */
    protected ?string $interface = CodeStandardInterface::class;

    /**
     * Information about standards available from the code standard services.
     *
     * @var array<string, string[]>|null
     */
    protected ?array $availStandards;

    /**
     * @param CliFactoryInterface $cliFactory
     */
    public function __construct(protected CliFactoryInterface $cliFactory)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function addHandler(CodeStandardInterface $handler, int $priority = 0): void
    {
        /** @var CodeStandardInterface $handler */
        $this->traitAddHandler($handler, $priority);

        if (isset($this->availStandards)) {
            $this->availStandards += $handler->getInfo();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAvailableStandards(): array
    {
        if (!isset($this->availStandards)) {
            $this->availStandards = [];

            foreach ($this->handlers as $standard) {
                $this->availStandards += $standard['handler']->getInfo();
            }
        }

        return $this->availStandards;
    }

    /**
     * {@inheritdoc}
     */
    public function getHandlerByStandard(string $standard): ?CodeStandardInterface
    {
        // Find the first handler that supports this standard in priority order.
        foreach ($this->getSortedHandlers() as $handler) {
            if ($handler->getInfo()[$standard] ?? null) {
                return $handler;
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getStandardByPath(PathInfo|array $paths, Tenant $tenant): ?array
    {
        if (!is_array($paths)) {
            $paths = [$paths];
        }

        $path = reset($paths);
        /** @var CodeStandardInterface $standard */
        foreach ($this->getSortedHandlers() as $standard) {
            if ($name = $standard->getStandardByPath($path, $tenant)) {
                // Ensure all the paths can be handled by the same handler,
                // otherwise there might be a mismatch with test framework.
                while (false !== ($path = next($paths))) {
                    if ($name !== $standard->getStandardByPath($path, $tenant)) {
                        throw new IncompatiblePathException($path, $name);
                    }
                }

                return [$name, $standard];
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(array|PathInfo $paths, Tenant $tenant, ?CodeStandardInterface $handler = null, ?string $standard = null, array $options = [], EnvironmentInterface|ExecutionContextInterface|null $context = null): int
    {
        if (!$handler) {
            if ($standard) {
                $handler = $this->getHandlerByStandard($standard);
            } else {
                @[$standard, $handler] = $this->getStandardByPath($paths, $tenant)['handler'];
            }

            // Still unable to determine which code standard handler to use.
            if (!$handler) {
                throw new \InvalidArgumentException('Unable to determine the coding standard to use with this command');
            }
        }

        // Run a customized execution of the code linter.
        if ($handler instanceof ExecutableStandardInterface) {
            return $handler->execute($standard, $paths, $tenant, $options, $context);
        }

        $args = [];
        $args[] = "--standard=".$handler->getStandardValue($standard, $tenant);

        // Setup ignore paths.
        if (!empty($options['ignore'])) {
            $args[] = '--ignore='.$options['ignore'];
        }

        // These flags are exclusive and handled separately.
        if ($options['explain']) {
            $args[] = '-e';
        } elseif ($options['generator']) {
            $args[] = "--generator={$options['generator']}";
        } else {
            if (is_array($paths)) {
                $args = array_merge($args, $paths);
            } else {
                $args[] = $paths;
            }
        }

        return $this->cliFactory
            ->create($handler->getCommand($tenant, $options), $context)
            ->setEnv(['XDEBUG_MODE' => 'off'])
            ->setTty(true)
            ->execute($args);
    }
}
